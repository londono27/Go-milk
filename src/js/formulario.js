const btnComprar = document.getElementById('btnComprar');


btnComprar.addEventListener('click', enviarInformacion);




function obtenerProductosLocalStorage() {
    let productoLS;

    //Comprobar si hay algo en LS
    if (localStorage.getItem('carrito') === null) {
        productoLS = {};
    } else {
        productoLS = JSON.parse(localStorage.getItem('carrito'));
    }
    return productoLS;
}

function obtenerPago() {
    let pagoLS;

    //Comprobar si hay algo en LS
    if (localStorage.getItem('pedido') === null) {
        pagoLS = {};
    } else {
        pagoLS = JSON.parse(localStorage.getItem('pedido'));
    }
    return pagoLS;
}


function obtenerCiudad() {
    let ciudad = document.getElementById('city');
    let ciudadSeleccionada = ciudad.value;
    return ciudadSeleccionada;
}


function validarCorreo(correo) {

    //Expresiones para el correo
    const expReg = /^[a-zA-Z0-9_.+-0]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    const esValidoCorreo = expReg.test(correo);

    if (esValidoCorreo == true) {
        validacionCorreo = true;
    }

    if (esValidoCorreo == false) {
        validacionCorreo = false;
    }
}


function validarCelular(numero) {

    //Expresioes para el celular
    const expReg = /^[0-9]{10}$/;
    const esValidoCelular = expReg.test(numero);

    if (esValidoCelular == true) {
        validacionCelular = true;
    }

    if (esValidoCelular == false) {
        validacionCelular = false;
    }
}


function compraRealizada(){   
    localStorage.clear();
    location.href='http://localhost/proyectos/GO-MILK/index.html';
    
}



function enviarInformacion() {

    productosLS = obtenerProductosLocalStorage();
    pagoLS = obtenerPago();
    ciudadSeleccionada = obtenerCiudad();

    const nombre = document.getElementById('name');
    const apellido = document.getElementById('lastname');
    const direccion = document.getElementById('adress');
    const celular = document.getElementById('phone');
    const correo = document.getElementById('email');
    validarCelular(celular.value);
    validarCorreo(correo.value);

    if(localStorage.getItem('carrito') === null||localStorage.getItem('carrito').length === 2 ){
        Swal.fire({
            title: 'El carrito esta vacío',
            icon: 'warning',
            iconColor: 'chocolate',
            confirmButtonColor: '#3b1e08',
            timer: 2000,
            timerProgressBar: true,
        })
    }else if (nombre.value == '' || apellido.value == '' || direccion.value == '' || celular.value == '' || correo.value == '') {

        Swal.fire({
            title: 'Por favor complete los campos',
            icon: 'warning',
            iconColor: 'chocolate',
            confirmButtonColor: '#3b1e08',
            timer: 2000,
            timerProgressBar: true,
        })

    } else if (validacionCelular == false) {

        Swal.fire({
            title: 'Por favor digite un celular valido',
            icon: 'warning',
            iconColor: 'chocolate',
            confirmButtonColor: '#3b1e08',
            timer: 2000,
            timerProgressBar: true,
        })


    } else if (validacionCorreo == false) {

        Swal.fire({
            title: 'Por favor digite un correo valido',
            icon: 'warning',
            iconColor: 'chocolate',
            confirmButtonColor: '#3b1e08',
            timer: 2000,
            timerProgressBar: true,
        })

    } else if(localStorage.getItem('pedido') === null){
        Swal.fire({
            title: 'Seleccione un medio de pago',
            icon: 'warning',
            iconColor: 'chocolate',
            confirmButtonColor: '#3b1e08',
            timer: 2000,
            timerProgressBar: true,
        })
        
    }else {


        Object.values(productosLS).forEach(function (producto) {
            $.post('./src/php/formulario.php', {
                'tituloProductos': producto.title,
                'cantidadProductos': producto.cantidad,
            });
        });

        $.post('./src/php/formulario.php', {
            'pago': pagoLS.medioPago
        });

        $.post('./src/php/formulario.php', {
            'ciudad': ciudadSeleccionada
        });


        Swal.fire({
            title: 'Gracias por su compra',
            icon: 'success',
            iconColor: 'gold',
            confirmButtonColor: '#3b1e08',
            timer: 2000,
            timerProgressBar: true,
        })

        setTimeout('compraRealizada()',2000);
    }

}