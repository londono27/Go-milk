# GO - MILK
### E-COMMERCE

[![N|Solid](https://cdn-icons-png.flaticon.com/512/2832/2832746.png)](https://nodesource.com/products/nsolid)



## !Bienvenido! 
Para ejecutar el proyecto necesitas de XAMPP y seguir los siguientes pasos:

- En la carpeta donde instalaste la aplicación XAMPP, encontraras una carpeta con el nombre de "htdocs", ahí debes colocar el proyecto completo.
- Posteriormente abrir XAMPP y darle "Start" al módulo de "Apache" y "MySQL", para luego ingresar en cualquier navegador a la siguiente página: http://localhost/phpmyadmin/
- Una vez hemos ingresado, debemos importar la base de datos del proyecto.
- Ingresamos a: http://localhost/GO-MILK/index.html y hemos ejecutado el proyecto ☕
- Si deseas ingresar al apartado de administrador, el usuario es: will y la contraseña es: 123

## Esperemos te guste 😊

### Tecnologías:
XAMPP, HTML, CSS, JavaScript y PHP.

### Hecho por:
Jose Miguel Londoño 
Handersson Felipe Pacheco Espitia

### Licencia:
MIT (Massachusetts Institute of Technology) LICENSE
### Anexos:
https://www.chartjs.org 
https://www.freepik.es/search?format=search&last_filter=type&last_value=icon&query=jun&type=icon 