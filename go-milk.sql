-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-03-2022 a las 22:29:44
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `go-milk`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `id` int(2) NOT NULL,
  `ciudad` char(40) NOT NULL,
  `cantidad` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id`, `ciudad`, `cantidad`) VALUES
(1, 'Bogotá', 231),
(2, 'Barranquilla', 52),
(3, 'Medellín', 25),
(4, 'Tunja', 7),
(5, 'Sogamoso', 91),
(6, 'Cali', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(2) NOT NULL,
  `pago` char(40) NOT NULL,
  `cantidad` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `pago`, `cantidad`) VALUES
(1, 'Efectivo', 77),
(2, 'Tarjeta', 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(2) NOT NULL,
  `titulo` char(40) NOT NULL,
  `precio` double(4,1) NOT NULL,
  `cantidad` int(40) NOT NULL,
  `preciototal` double(255,1) NOT NULL,
  `ganancia` double(4,1) NOT NULL,
  `gananciatotal` double(255,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `titulo`, `precio`, `cantidad`, `preciototal`, `ganancia`, `gananciatotal`) VALUES
(1, 'Cortado', 2.5, 72, 180.0, 1.5, 108.0),
(2, 'Expresso', 2.5, 5, 12.5, 1.5, 7.5),
(3, 'Macchiato', 3.0, 4, 12.0, 1.3, 5.2),
(4, 'Coretto', 3.5, 0, 0.0, 1.6, 0.0),
(5, 'Con panna', 4.0, 40, 160.0, 1.8, 72.0),
(6, 'Romano', 3.5, 18, 63.0, 1.6, 28.8),
(7, 'Mead raf', 3.0, 0, 0.0, 1.3, 0.0),
(8, 'Marocchino', 2.5, 0, 0.0, 1.5, 0.0),
(9, 'Mocha', 3.5, 0, 0.0, 1.6, 0.0),
(10, 'Breve', 2.5, 1, 2.5, 1.5, 1.5),
(11, 'Raf coffe', 3.0, 0, 0.0, 1.3, 0.0),
(12, 'Chocolate Milk', 4.0, 80, 0.0, 1.8, 0.0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(50) NOT NULL COMMENT 'Nombre de usuario',
  `contra` varchar(50) NOT NULL COMMENT 'Entrada de PW'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `contra`) VALUES
('will', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores`
--

CREATE TABLE `valores` (
  `id` int(2) NOT NULL,
  `ingresos` double(255,1) NOT NULL,
  `ventas` int(255) NOT NULL,
  `beneficios` double(255,1) NOT NULL,
  `gastos` double(255,1) NOT NULL,
  `mayorProducto` char(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `valores`
--

INSERT INTO `valores` (`id`, `ingresos`, `ventas`, `beneficios`, `gastos`, `mayorProducto`) VALUES
(1, 430.0, 220, 223.0, 0.0, 'Chocolate Milk');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- Indices de la tabla `valores`
--
ALTER TABLE `valores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `valores`
--
ALTER TABLE `valores`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
